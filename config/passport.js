var LocalStrategy = require('passport-local').Strategy;
var User = require('../app/models/user');
var FacebookStrategy = require('passport-facebook').Strategy;
var configAuth = require('./auth');
var mysql = require("mysql");
var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "root",
  database: "node"
});

module.exports = function(passport){
 
   passport.serializeUser(function(user, done){

   	   done(null,user);
   });

   passport.deserializeUser(function(user, done){
          
          done(null, user);
   });


   passport.use('local-signup', new LocalStrategy({
		usernameField: 'email',
		passwordField: 'password',
		passReqToCallback: true
	},
	function(req, email, password, done){
		process.nextTick(function(){
			var sql = "SELECT * FROM user Where email = "+"'"+email+"'";
			con.query(sql, function(err, users){
				if(err)
					return done(err);
				if(users.Count > 0){
					return done(null, false, req.flash('signupMessage', 'That email already taken'));
				} else {
					var newUser = new User();
					newUser.local.email = email;
					newUser.local.password = password;

					var sql = "INSERT INTO user (email, password) VALUES ("+"'"+email+"'"+", "+"'"+password+"')";
                    con.query(sql, function (err, result) {
                    if (err) return done(err);
                    return done(null,newUser);
                    });
				}
			})

		});
	}));


   passport.use('local-login', new LocalStrategy({
			usernameField: 'email',
			passwordField: 'password',
			passReqToCallback: true
		},
		function(req, email, password, done){
			process.nextTick(function(){
				var sql = "SELECT * FROM user Where email = "+"'"+email+"'";
				con.query(sql, function(err, users){
					if(err)
						return done(err);
					if(users.Count == 0)
						return done(null, false, req.flash('loginMessage', 'No User found'));
					if(users[0].password != password){
						return done(null, false, req.flash('loginMessage', 'invalid password'));
					}
					var user = new User();
					user.local.email = users[0].email;
					user.local.password = users[0].password;
					return done(null, user);

				});
			});
		}
	));

   passport.use(new FacebookStrategy({
    clientID: configAuth.facebookAuth.clientID,
    clientSecret: configAuth.facebookAuth.clientSecret,
    callbackURL: configAuth.facebookAuth.callbackURL,
    profileFields: ['id', 'displayName', 'photos', 'email']
  },
  function(accessToken, refreshToken, profile, done) {
	    	process.nextTick(function(){
	    		var sql = "SELECT * FROM user Where id = "+"'"+profile.id+"'";
				con.query(sql, function(err, users){
	    			if(err)
	    				return done(err);
	    			if(users.Count == 1){
	    				var user = new User();
	    			
					user.facebook.id = users[0].id;
					user.facebook.token = users[0].token;
					user.facebook.name = users[0].name;
					user.facebook.email = users[0].email;
					user.facebook.photos = users[0].photos;
	    				return done(null, user);}
	    			else {
	    				var user = new User();
	    				user.facebook.id = profile.id;
	    				user.facebook.token = accessToken;
	    				user.facebook.name = profile.name.givenName + ' ' + profile.name.familyName;
	    				user.facebook.photos = profile.photos[0].value;
	    			//	newUser.facebook.email = profile.emails[0].value;

	    				var sql = "INSERT INTO user (id,token,name,email) VALUES ("+"'"+user.facebook.id+"'"+","+"'"+user.facebook.token+"'"+","+"'"+user.facebook.name+"'"+", "+"'"+user.facebook.email+"')";
                    con.query(sql, function (err, result) {
                    if (err) return done(err);
                    return done(null,user);
                    });
	    				console.log(profile);
	    			}
	    		});
	    	});
	    }
));

}