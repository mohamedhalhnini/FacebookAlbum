var express = require('express');
var app = express();
var port = process.env.PORT || 1339;

var morgan = require('morgan');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var passport = require('passport');
var flash = require('connect-flash');
require('./config/passport')(passport);

app.use(morgan('dev'));
app.use(cookieParser());
app.use(bodyParser.urlencoded({extended: false}))
app.use(session({secret : "simo",
                 saveUninitialized : true,
                 resave : true}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

app.set('view engine', 'ejs');
/*app.use('/', function(req, res){
   res.send('Our first program');
})*/

require('./app/routes.js')(app, passport);

app.listen(port);
console.log('server' + port);